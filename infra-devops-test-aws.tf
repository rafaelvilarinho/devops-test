terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  version = ">= 2.0.0"
  region  = "us-east-2"
}

data "aws_availability_zones" "available" {}

locals {
  cluster_name = "devops-test-cluster"
  k8s_cluster_addr = "kubernetes.io/cluster/devops-test-cluster"

  worker_groups = [
    {
      instance_type        = "t2.micro" 
      asg_desired_capacity = "1"        
      asg_max_size         = "1"        
      asg_min_size         = "1"        
      autoscaling_enabled  = false
    },
  ]

  map_users = [
    {
      userarn = "arn:aws:iam::271446716176:user/devops-test-user"
      username = "devops-test-user"
      groups    = ["system:masters"]
    },
  ]

  tags = {
    Environment   = "DEVOPS_TEST_MUTANT"
    creation-date = timestamp()
  }
}

module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  name               = "devops-test-cluster-vpc"
  cidr               = "10.0.0.0/16"
  azs                = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1], data.aws_availability_zones.available.names[2]]
  private_subnets    = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  enable_nat_gateway = true
  single_nat_gateway = true
  tags               = merge(local.tags, map(local.k8s_cluster_addr, "shared"))
}

module "eks" {
  source                      = "terraform-aws-modules/eks/aws"
  cluster_name                = local.cluster_name
  subnets                     = module.vpc.private_subnets
  tags                        = local.tags
  vpc_id                      = module.vpc.vpc_id
  worker_groups               = local.worker_groups
  map_users                   = local.map_users
  worker_sg_ingress_from_port = "0"
}
