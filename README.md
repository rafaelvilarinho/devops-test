## Teste DevOps

O projeto está implantado na AWS, utilizando ECR e EKS. Utilizei o Terraform para gerar a infraestrutura do projeto e as pipelines do Bitbucket para o CI/CD.

Quando a master recebe um commit, a aplicação é atualizada em:

- http://a9bfe99e4772440d7af18c61e9e53598-1534573437.us-east-2.elb.amazonaws.com:3000